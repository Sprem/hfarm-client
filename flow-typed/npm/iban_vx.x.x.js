// flow-typed signature: 4e0b3b05b06f7d7f0a166a6867560f7a
// flow-typed version: <<STUB>>/iban_v0.0.8/flow_v0.46.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'iban'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'iban' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'iban/iban' {
  declare module.exports: any;
}

declare module 'iban/test/ibanTest' {
  declare module.exports: any;
}

// Filename aliases
declare module 'iban/iban.js' {
  declare module.exports: $Exports<'iban/iban'>;
}
declare module 'iban/test/ibanTest.js' {
  declare module.exports: $Exports<'iban/test/ibanTest'>;
}
