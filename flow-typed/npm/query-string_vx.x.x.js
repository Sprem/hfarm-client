// flow-typed signature: 3e7ff205d7abdf0943468c6df36404be
// flow-typed version: <<STUB>>/query-string_v4.3.4/flow_v0.46.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'query-string'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'query-string' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'query-string/index' {
  declare module.exports: $Exports<'query-string'>;
}
declare module 'query-string/index.js' {
  declare module.exports: $Exports<'query-string'>;
}
