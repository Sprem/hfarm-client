// flow-typed signature: 68f2124900aa00f3ed975715a9d890df
// flow-typed version: <<STUB>>/normalize.css_v^7.0.0/flow_v0.46.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'normalize.css'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'normalize.css' {
  declare module.exports: any;
}
