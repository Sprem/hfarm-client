import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import App from './App';
import 'normalize.css';
import './Theme/variables.css';
import './Theme/grid.css';
import './Theme/typography.css';
import './Theme/layout.css';
import './Theme/shared-components.css';
import './Theme/Autocomplete.css';
injectTapEventPlugin();

console.log("Starting");

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
