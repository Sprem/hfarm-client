// @flow
import type { GlobalUi } from '../../Types/GlobalUi';
import type { Forms } from '../../Types/Forms';
import type { CreateJobForm } from '../../Types/Job';
import type { RouterHistory } from 'react-router-dom';

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'flux/utils';
import { Paper, RaisedButton, DatePicker, Toggle } from 'material-ui';
import TextFieldWithValidation from '../../Components/TextFieldWithValidation';
import Toolbar from '../../Components/Toolbar';
// stores
import GlobalUiStore from '../../Stores/GlobalUi';
import FormsStore, { createJobFormId } from '../../Stores/Forms';
// actions
import GlobalUiActions from '../../Actions/GlobalUi';
import FormsActions from '../../Actions/Forms';
// libs
import Paths from '../../App/Paths';
import * as Validations from '../../Services/Validations';
import { propAtPath } from '../../Services/Helpers';
import * as ServiceJob from '../../Services/Api/Job';
import i18n from 'i18next';
import '../../Theme/LoginPage.css';
import dateFns from 'date-fns';
import { DateTimeFormat } from 'intl';
import * as Helpers from '../../Services/Helpers';
import './index.css';

type Props = {
  history: RouterHistory
};

type State = {
  globalUi: GlobalUi,
  forms: Forms,
  studentUser: StudentUser
};

class NewJobPage extends Component {
  props: Props;
  state: State;

  static defaultProps: {};

  static getStores() {
    return [ GlobalUiStore, FormsStore ];
  };

  static calculateState() {
    return {
      globalUi: GlobalUiStore.getState(),
      forms: FormsStore.getState()
    }
  };

  onSubmit = () => {
    const form = this.state.forms[createJobFormId];
    FormsActions.formValidate(createJobFormId);

    const isFormValid = Validations.validateAll([
      () => Validations.validateFilled(form.title,3),
      () => Validations.validateFilled(form.description, 3),
      () => Validations.validateFilled(form.clientName, 3)
    ]);

    if(isFormValid) {

        ServiceJob.createJob(
            form,
            (data) => {
                FormsActions.formReset(createJobFormId);
                this.props.history.push(Paths.user.dashboard);
            },
            (error) => {
              GlobalUiActions.globalUiAsyncErrorUpdate({
                message: propAtPath(error, 'response.data.message'),
                actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
                autoHide: false,
                onRequestClose: function() {
                  GlobalUiActions.globalUiAsyncErrorReset();
                }
              });
            }
        );

    } else {
      GlobalUiActions.globalUiAsyncErrorUpdate({
        message: i18n.t('Pages.LoginPage.form.invalidForm'),
        actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
        autoHide: false,
        onRequestClose: function() {
          GlobalUiActions.globalUiAsyncErrorReset();
        }
     });

    }

  }

  handleEnterKeyDown = (event: KeyboardEvent) => {
    if(event.which === 13 || event.keyCode === 13){
      this.onSubmit();
    } else {
      /* do nothing */
    }
  }

  componentWillMount() {
    document.addEventListener('keydown', this.handleEnterKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleEnterKeyDown)
  }

  render() {
    const form = this.state.forms[createJobFormId];

    return(
      <div>
        <Toolbar noShadow={true}/>
        <div className="page AuthPage">
          <div className="AuthPage__logo">
          </div>
          <Paper zDepth={1} className="NewJobPage__paper">
            <h2>{i18n.t('Pages.NewJobPage.title')}</h2>
            <TextFieldWithValidation
              id="NewJobPage__title"
              value={form.title}
              fullWidth={true}
              multiLine={false}
              hintText=""
              errorText={i18n.t('Pages.NewJobPage.form.titleValidation')}
              floatingLabelText={i18n.t('Pages.NewJobPage.form.titleLabel')}
              showValidations={form.validated}
              fieldKey={'title'}
              type={'email'}
              focusedInput={form.focusedInput}
              validation={(value) => Validations.validateFilled(value,3)}
              onChange={(event, value) => FormsActions.formInputChange(createJobFormId, 'title', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createJobFormId, 'title')}}
              onBlur={(event) => FormsActions.formBlur(createJobFormId)}
            />
            <TextFieldWithValidation
              id="NewJobPage__description"
              value={form.description}
              fullWidth={true}
              multiLine={true}
              hintText=""
              errorText=""
              floatingLabelText={i18n.t('Pages.NewJobPage.form.descriptionLabel')}
              helpText={i18n.t('Pages.NewJobPage.form.descriptionAdditionalInfo')}
              showValidations={form.validated}
              fieldKey={'descriptions'}
              focusedInput={form.focusedInput}
              validation={(value) => Validations.validateFilled(value,3)}
              onChange={(event, value) => FormsActions.formInputChange(createJobFormId, 'description', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createJobFormId, 'description')}}
              onBlur={(event) => FormsActions.formBlur(createJobFormId)}
            />
            <TextFieldWithValidation
              id="NewJobPage__clientName"
              value={form.clientName}
              fullWidth={true}
              multiLine={true}
              hintText=""
              errorText=""
              floatingLabelText={i18n.t('Pages.NewJobPage.form.clientNameLabel')}
              showValidations={form.validated}
              fieldKey={'clientName'}
              focusedInput={form.focusedInput}
              validation={(value) => Validations.validateFilledMax(value,3,40)}
              onChange={(event, value) => FormsActions.formInputChange(createJobFormId, 'clientName', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createJobFormId, 'clientName')}}
              onBlur={(event) => FormsActions.formBlur(createJobFormId)}
            />
            <TextFieldWithValidation
              id="NewJobPage__experience"
              value={form.experience}
              fullWidth={true}
              multiLine={true}
              hintText=""
              errorText=""
              floatingLabelText={i18n.t('Pages.NewJobPage.form.experienceLabel')}
              fieldKey={'experience'}
              focusedInput={form.focusedInput}
              onChange={(event, value) => FormsActions.formInputChange(createJobFormId, 'experience', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createJobFormId, 'experience')}}
              onBlur={(event) => FormsActions.formBlur(createJobFormId)}
            />
            <DatePicker
              value={form.startingDate}
              fullWidth={true}
              className="NewJobPage__startingDate"
              autoOk={true}
              floatingLabelText={i18n.t('Pages.NewJobPage.form.startingTimeLabel')}
              mode={Helpers.isSmallScreen() ? "portrait" : "landscape"}
              onChange={(_, date) => FormsActions.formInputChange(createJobFormId, 'startingDate', date)}
              defaultDate={form.startingDate}
              minDate={dateFns.addDays(new Date(), 0)}
              formatDate={(date) => date && dateFns.format(date, 'YYYY-MM-DD')}
              okLabel={false}
              locale="it"
              DateTimeFormat={DateTimeFormat}
            />
            <DatePicker
              value={form.closingDate}
              fullWidth={true}
              className="NewJobPage__endingDate"
              autoOk={true}
              floatingLabelText={i18n.t('Pages.NewJobPage.form.endingTimeLabel')}
              mode={Helpers.isSmallScreen() ? "portrait" : "landscape"}
              onChange={(_, date) => FormsActions.formInputChange(createJobFormId, 'closingDate', date)}
              defaultDate={form.closingDate}
              minDate={dateFns.addDays(new Date(), 1)}
              formatDate={(date) => date && dateFns.format(date, 'DD/MM/YYYY')}
              okLabel={false}
              locale="it"
              DateTimeFormat={DateTimeFormat}
            />
            <Toggle
              id="NewJobPage__isInternal"
              className="NewJobPage__isInternal"
              label={i18n.t('Pages.NewJobPage.form.isInternatLabel.'+form.isInternal)}
              labelPosition="right"
              toggled={form.isInternal}
              onToggle={(event, isToggled) => FormsActions.formInputChange(createJobFormId, 'isInternal', isToggled)}
            />
            <RaisedButton
              className="NewJobPag__signup-button"
              label={i18n.t('Pages.NewJobPage.form.submit')}
              primary={true}
              fullWidth={true}
              onTouchTap={this.onSubmit}
            />
          </Paper>
        </div>
      </div>
    )
  };
}

export default Container.create(NewJobPage);
