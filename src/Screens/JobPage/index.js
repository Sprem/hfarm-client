// @flow
import type { GlobalUi } from '../../Types/GlobalUi';

import React, { Component } from 'react';
import Toolbar from '../../Components/Toolbar';
import { Container } from 'flux/utils';
import { FontIcon } from 'material-ui';
// stores
import GlobalUiStore from '../../Stores/GlobalUi';
// libs
import Paths from '../../App/Paths';


type Props = {
  history: RouterHistory
};

type State = {
  globalUi: GlobalUi
};

class JobPage extends Component {
  props: Props;
  state: State;

  static defaultProps: {};

  static getStores() {
    return [ GlobalUiStore ];
  };

  static calculateState() {
    return {
      globalUi: GlobalUiStore.getState()
    }
  };

  render() {

    return(
      <div>
        <Toolbar noShadow={true}/>
        <div style={{width: '900px', marginLeft: 'auto', marginRight: 'auto'}}>
        <FontIcon style={{fontSize: '300px', marginTop: '150px'}} className="material-icons Dashboard_image_label_icon" >sentiment_satisfied_alt</FontIcon>
        <FontIcon style={{fontSize: '300px', marginTop: '150px'}} className="material-icons Dashboard_image_label_icon" >sentiment_satisfied_alt</FontIcon>
        <FontIcon style={{fontSize: '300px', marginTop: '150px'}} className="material-icons Dashboard_image_label_icon" >sentiment_satisfied_alt</FontIcon>
      </div></div>
    )
  };
}

export default Container.create(JobPage);
