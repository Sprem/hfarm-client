// @flow
import type { GlobalUi } from '../../Types/GlobalUi';
import type { Forms } from '../../Types/Forms';
import type { RouterHistory } from 'react-router-dom';

import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { Paper, RaisedButton } from 'material-ui';
import TextFieldWithValidation from '../../Components/TextFieldWithValidation';
// stores
import GlobalUiStore from '../../Stores/GlobalUi';
import FormsStore, { loginFormId } from '../../Stores/Forms';

// actions
import GlobalUiActions from '../../Actions/GlobalUi';
import FormsActions from '../../Actions/Forms';
// libs
import Paths from '../../App/Paths';
import * as Validations from '../../Services/Validations';
import { propAtPath } from '../../Services/Helpers';
import * as ServiceUser from '../../Services/Api/User';
import i18n from 'i18next';
import '../../Theme/LoginPage.css';

type Props = {
  history: RouterHistory
};

type State = {
  globalUi: GlobalUi,
  forms: Forms,
  studentUser: StudentUser
};

class LoginPage extends Component {
  props: Props;
  state: State;

  static defaultProps: {};

  static getStores() {
    return [ GlobalUiStore, FormsStore ];
  };

  static calculateState() {
    return {
      globalUi: GlobalUiStore.getState(),
      forms: FormsStore.getState()
    }
  };

  onSubmit = () => {
    const form = this.state.forms[loginFormId];
    FormsActions.formValidate(loginFormId);

    const isFormValid = Validations.validateAll([
      () => Validations.validateFilled(form.email) && Validations.validateEmail(form.email || ''),
      () => Validations.validateFilled(form.password, 6)
    ]);

    if(isFormValid) {

        ServiceUser.loginUserAPI(
            form,
            (data) => {
                this.props.history.push(Paths.user.dashboard);

            },
            (error) => {
                console.log(error)
              GlobalUiActions.globalUiAsyncErrorUpdate({
                message: propAtPath(error, 'response.data.message'),
                actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
                autoHide: false,
                onRequestClose: function() {
                  GlobalUiActions.globalUiAsyncErrorReset();
                }
              });
            }
        );

    } else {
      GlobalUiActions.globalUiAsyncErrorUpdate({
        message: i18n.t('Pages.LoginPage.form.invalidForm'),
        actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
        autoHide: false,
        onRequestClose: function() {
          GlobalUiActions.globalUiAsyncErrorReset();
        }
      });

    }
  }

  handleEnterKeyDown = (event: KeyboardEvent) => {
    if(event.which === 13 || event.keyCode === 13){
      this.onSubmit();
    } else {
      /* do nothing */
    }
  }

  componentWillMount() {
    document.addEventListener('keydown', this.handleEnterKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleEnterKeyDown)
  }

  render() {
    const form = this.state.forms[loginFormId];

    return(
      <div>
        <div className="page AuthPage">
          <div className="AuthPage__logo">
            <img className="LoginLogo" src="/images/mistralLogoText.png" />
          </div>
          <Paper zDepth={1} className="AuthPage__paper">
            <h2>{i18n.t('Pages.LoginPage.title')}</h2>
            <TextFieldWithValidation
              id="LoginPage__email"
              value={form.email}
              fullWidth={true}
              multiLine={false}
              hintText=""
              errorText={i18n.t('Pages.LoginPage.form.emailValidation')}
              floatingLabelText={i18n.t('Pages.LoginPage.form.emailLabel')}
              showValidations={form.validated}
              fieldKey={'email'}
              type={'email'}
              focusedInput={form.focusedInput}
              validation={(value) => Validations.validateFilled(value) && Validations.validateEmail(value)}
              onChange={(event, value) => FormsActions.formInputChange(loginFormId, 'email', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(loginFormId, 'email')}}
              onBlur={(event) => FormsActions.formBlur(loginFormId)}
            />
            <div className="LoginPage__password-wrapper">
              <TextFieldWithValidation
                id="LoginPage__password"
                value={form.password}
                type="password"
                fullWidth={true}
                multiLine={false}
                hintText=""
                errorText={i18n.t('Pages.LoginPage.form.passwordValidation')}
                floatingLabelText={i18n.t('Pages.LoginPage.form.passwordLabel')}
                showValidations={form.validated}
                fieldKey={'password'}
                focusedInput={form.focusedInput}
                validation={(value) => Validations.validateFilled(value, 6)}
                onChange={(event, value) => FormsActions.formInputChange(loginFormId, 'password', value)}
                onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(loginFormId, 'password')}}
                onBlur={(event) => FormsActions.formBlur(loginFormId)}
              />
            </div>
            <RaisedButton
              className="LoginPage__signup-button"
              label={i18n.t('Pages.LoginPage.form.login')}
              primary={true}
              fullWidth={true}
              onTouchTap={this.onSubmit}
            />
          </Paper>
        </div>
      </div>
    )
  };
}

export default Container.create(LoginPage);
