// @flow
import type { Jobs } from '../../Types/Job';
import type { RouterHistory } from 'react-router-dom';
import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { FontIcon } from 'material-ui';
import Toolbar from '../../Components/Toolbar';
import { Link } from 'react-router-dom';
// stores
import JobStore from '../../Stores/Job';
// actions
import GlobalUiActions from '../../Actions/GlobalUi';
// libs
import Paths from '../../App/Paths';
import { propAtPath } from '../../Services/Helpers';
import * as ServiceJob from '../../Services/Api/Job';
import i18n from 'i18next';
import { noop } from '../../Services/Helpers';
import './index.css';
import '../../Theme/boot-grid.css';

type Props = {
  history: RouterHistory
}

type State = {
  jobs: Jobs
}

class DashboardPage extends Component {
  props: Props;
  state: State;

  static defaultProps: {};

  static getStores() {
    return [ JobStore ];
  };

  static calculateState() {
    return {
      jobs: JobStore.getState()
    }
  };

  getJobs = () => {
      setTimeout(function() {
          ServiceJob.listJob(
            noop,
            (error) => {
              GlobalUiActions.globalUiAsyncErrorUpdate({
                message: propAtPath(error, 'response.data.message_it'),
                actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
                autoHide: false,
                onRequestClose: function() {
                  GlobalUiActions.globalUiAsyncErrorReset();
                }
              });
            }
          )
      }, 0)
  }

  componentDidMount() {
      this.getJobs();
  }

  createStruct = () => {
      var totalJob = this.state.jobs.jobs.length + 1;
      var totalRow = parseInt(totalJob / 4);
      if(totalJob % 4 > 0) totalRow++;

      var struct = new Array(totalRow);
      for(var i = 0; i < totalRow; i++) { struct[i] = new Array(4); }

      for(var i = 0; i < totalJob; i++){
          if(i == 0){
              struct[0][0] = "ADD";
          }else{
              var row = parseInt(i / 4);
              var col = parseInt(i - (4*row));
              if(this.state.jobs.jobs[i-1].title.length > 70)
                this.state.jobs.jobs[i-1].title = this.state.jobs.jobs[i-1].title.substring(0,70) + "...";
              struct[row][col] = this.state.jobs.jobs[i-1];
          }
      }
      return struct;
  }

  render() {

    const struct = this.createStruct();

    return(
        <div>
            <Toolbar noShadow={true}/>
            <div className="Dashboard_main_wrapper">
                {
                    struct.map ((row, index) => {
                        return <div className="row" key={index}>

                            {
                                row.map((col,index) => {
                                    if(col == "ADD") return <div className="product col-sm-6 col-md-3 col-lg-3 Dashboard_grid_row">
                                        <Link to={Paths.user.createJob} key={index + 100}>
                                            <div className="Dashboard_card">
                                                <div style={{textAlign: 'center'}} className="Dashboard_card_body">
                                                    <FontIcon style={{fontSize: '60px'}} className="material-icons Dashboard_image_label_icon" >add</FontIcon>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>
                                    else return <div className="product col-sm-6 col-md-3 col-lg-3 Dashboard_grid_row">
                                        <Link to={Paths.user.job} key={index + 100}>
                                            <div className="Dashboard_card">
                                                {col.isInternal ? <div className="Dashboard_card_header_orange">internal</div> : <div className="Dashboard_card_header_blue">{col.clientName}</div>}
                                                <div className="Dashboard_card_body">
                                                    {col.title}
                                                </div>
                                            </div>
                                        </Link>
                                    </div>

                                })
                            }

                        </div>;
                    })
                }
            </div>
        </div>
    )
  };
}

export default Container.create(DashboardPage);
