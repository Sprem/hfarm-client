// @flow
import type { GlobalUi } from '../../Types/GlobalUi';
import type { Forms } from '../../Types/Forms';
import type { RouterHistory } from 'react-router-dom';

import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { Paper, RaisedButton, DatePicker } from 'material-ui';
import TextFieldWithValidation from '../../Components/TextFieldWithValidation';
import Toolbar from '../../Components/Toolbar';
// stores
import CandidateStore from '../../Stores/Candidate';
import FormsStore, { createCandidateFormId } from '../../Stores/Forms';
import GlobalUiStore from '../../Stores/GlobalUi';
// actions
import GlobalUiActions from '../../Actions/GlobalUi';
import FormsActions from '../../Actions/Forms';
// libs
import Paths from '../../App/Paths';
import * as Validations from '../../Services/Validations';
import { propAtPath } from '../../Services/Helpers';
import * as ServiceCandidate from '../../Services/Api/Candidate';
import i18n from 'i18next';
import '../../Theme/LoginPage.css';
import dateFns from 'date-fns';
import { DateTimeFormat } from 'intl';
import * as Helpers from '../../Services/Helpers';
import './index.css';
import { noop, YYYYMMDDToTaskhunterDateFormat } from '../../Services/Helpers';

type Props = {
  history: RouterHistory
};

type State = {
  globalUi: GlobalUi,
  candidates: Candidates,
  forms: Forms
};

class UpdateCandidatePage extends Component {
  props: Props;
  state: State;

  static defaultProps: {};

  static getStores() {
    return [ CandidateStore, FormsStore, GlobalUiStore ];
  };

  static calculateState() {
    return {
      candidates: CandidateStore.getState(),
      forms: FormsStore.getState(),
      globalUi: GlobalUiStore.getState()
    }
  };

  getCurrentCandidateId = () => {
    return propAtPath(this.props.match, 'params.id');
  }

  getCandidate = () => {

      var idApplicant = this.getCurrentCandidateId();
      setTimeout(function() {
          ServiceCandidate.getCandidate(
            idApplicant,
            noop,
            (error) => {
              GlobalUiActions.globalUiAsyncErrorUpdate({
                message: propAtPath(error, 'response.data.message_it'),
                actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
                autoHide: false,
                onRequestClose: function() {
                  GlobalUiActions.globalUiAsyncErrorReset();
                }
              });
            }
          )
      }, 0)
  }

  onSubmit = () => {
    const form = this.state.forms[createCandidateFormId];
    FormsActions.formValidate(createCandidateFormId);

    const isFormValid = Validations.validateAll([
      () => Validations.validateFilled(form.email) && Validations.validateEmail(form.email || ''),
      () => Validations.validateFilled(form.firstName, 3),
      () => Validations.validateFilled(form.lastName, 2),
      () => Validations.validateFilled(form.role, 2),
      () => Validations.validateFilled(form.seniority, 2)
    ]);

    if(isFormValid) {

        var idApplicant = this.getCurrentCandidateId();
        ServiceCandidate.updateCandidate(
            idApplicant,
            form,
            (data) => {
                GlobalUiActions.globalUiAsyncErrorUpdate({
                  message: i18n.t('Pages.UpdateCandidatePage.form.success'),
                  actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
                  autoHide: false,
                  onRequestClose: function() {
                    GlobalUiActions.globalUiAsyncErrorReset();
                  }
                });
            },
            (error) => {
              GlobalUiActions.globalUiAsyncErrorUpdate({
                message: propAtPath(error, 'response.data.message'),
                actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
                autoHide: false,
                onRequestClose: function() {
                  GlobalUiActions.globalUiAsyncErrorReset();
                }
              });
            }
        );

    } else {
      GlobalUiActions.globalUiAsyncErrorUpdate({
        message: i18n.t('Pages.LoginPage.form.invalidForm'),
        actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
        autoHide: false,
        onRequestClose: function() {
          GlobalUiActions.globalUiAsyncErrorReset();
        }
     });

    }

  }

  componentDidMount() {
      this.getCandidate();
  }

  render() {
    const form = this.state.forms[createCandidateFormId];

    return(
      <div>
        <Toolbar noShadow={true}/>
        <div className="page AuthPage">
          <div className="AuthPage__logo">
          </div>
          <Paper zDepth={1} className="UpdateCandidatePage__paper">
            <h2>{i18n.t('Pages.UpdateCandidatePage.title')}</h2>
            <TextFieldWithValidation
              id="UpdateCandidate__firstName"
              value={form.firstName}
              fullWidth={true}
              multiLine={false}
              hintText=""
              errorText={i18n.t('Pages.UpdateCandidatePage.form.firstNameValidation')}
              floatingLabelText={i18n.t('Pages.UpdateCandidatePage.form.firstNameLabel')}
              showValidations={form.validated}
              fieldKey={'firstName'}
              focusedInput={form.focusedInput}
              validation={(value) => Validations.validateFilledMax(value,3,40)}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'firstName', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'firstName')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <TextFieldWithValidation
              id="UpdateCandidate__lastName"
              value={form.lastName}
              fullWidth={true}
              multiLine={true}
              hintText=""
              errorText={i18n.t('Pages.UpdateCandidatePage.form.lastNameValidation')}
              floatingLabelText={i18n.t('Pages.UpdateCandidatePage.form.lastNameLabel')}
              showValidations={form.validated}
              fieldKey={'lastName'}
              focusedInput={form.focusedInput}
              validation={(value) => Validations.validateFilledMax(value,2,40)}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'lastName', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'lastName')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <TextFieldWithValidation
              id="UpdateCandidate__email"
              value={form.email}
              fullWidth={true}
              multiLine={false}
              hintText=""
              errorText={i18n.t('Pages.UpdateCandidatePage.form.emailValidation')}
              floatingLabelText={i18n.t('Pages.UpdateCandidatePage.form.emailLabel')}
              showValidations={form.validated}
              fieldKey={'email'}
              type={'email'}
              focusedInput={form.focusedInput}
              validation={(value) => Validations.validateFilled(value) && Validations.validateEmail(value)}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'email', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'email')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <DatePicker
              value={new Date(form.dob)}
              fullWidth={true}
              className="UpdateCandidate__dob"
              autoOk={true}
              floatingLabelText={i18n.t('Pages.UpdateCandidatePage.form.dobLabel')}
              mode={Helpers.isSmallScreen() ? "portrait" : "landscape"}
              onChange={(_, date) => FormsActions.formInputChange(createCandidateFormId, 'dob', date)}
              defaultValue={new Date(form.dob)}
              formatDate={(date) => date && dateFns.format(date, 'DD/MM/YYYY')}
              okLabel={false}
              locale="it"
              format={null}
              DateTimeFormat={DateTimeFormat}
            />
            <TextFieldWithValidation
              id="UpdateCandidate__country"
              value={form.country}
              fullWidth={true}
              multiLine={false}
              hintText=""
              floatingLabelText={i18n.t('Pages.UpdateCandidatePage.form.countryLabel')}
              fieldKey={'country'}
              focusedInput={form.focusedInput}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'country', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'country')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <TextFieldWithValidation
              id="UpdateCandidate__role"
              value={form.role}
              fullWidth={true}
              multiLine={false}
              hintText=""
              floatingLabelText={i18n.t('Pages.UpdateCandidatePage.form.roleLabel')}
              errorText={i18n.t('Pages.UpdateCandidate.form.roleValidation')}
              fieldKey={'role'}
              showValidations={form.validated}
              validation={(value) => Validations.validateFilledMax(value,3,40)}
              focusedInput={form.focusedInput}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'role', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'role')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <TextFieldWithValidation
              id="UpdateCandidate__seniority"
              value={form.seniority}
              fullWidth={true}
              multiLine={false}
              hintText=""
              floatingLabelText={i18n.t('Pages.UpdateCandidatePage.form.seniorityLabel')}
              errorText={i18n.t('Pages.UpdateCandidatePage.form.seniorityValidation')}
              fieldKey={'seniority'}
              showValidations={form.validated}
              validation={(value) => Validations.validateFilledMax(value,3,40)}
              focusedInput={form.focusedInput}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'seniority', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'seniority')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <RaisedButton
              className="UpdateCandidatePage__signup-button"
              label={i18n.t('Pages.UpdateCandidatePage.form.submit')}
              primary={true}
              fullWidth={true}
              onTouchTap={this.onSubmit}
            />
          </Paper>
        </div>
      </div>
    )
  };
}

export default Container.create(UpdateCandidatePage);
