// @flow
import type { GlobalUi } from '../../Types/GlobalUi';
import type { Forms } from '../../Types/Forms';
import type { CreateCandidateForm } from '../../Types/Candidate';
import type { RouterHistory } from 'react-router-dom';

import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { Paper, RaisedButton, DatePicker } from 'material-ui';
import TextFieldWithValidation from '../../Components/TextFieldWithValidation';
import Toolbar from '../../Components/Toolbar';
// stores
import GlobalUiStore from '../../Stores/GlobalUi';
import FormsStore, { createCandidateFormId } from '../../Stores/Forms';
// actions
import GlobalUiActions from '../../Actions/GlobalUi';
import FormsActions from '../../Actions/Forms';
// libs
import Paths from '../../App/Paths';
import * as Validations from '../../Services/Validations';
import { propAtPath } from '../../Services/Helpers';
import * as ServiceCandidate from '../../Services/Api/Candidate';
import i18n from 'i18next';
import '../../Theme/LoginPage.css';
import dateFns from 'date-fns';
import { DateTimeFormat } from 'intl';
import * as Helpers from '../../Services/Helpers';
import './index.css';

type Props = {
  history: RouterHistory
};

type State = {
  globalUi: GlobalUi,
  forms: Forms,
  studentUser: StudentUser
};

class NewCandidatePage extends Component {
  props: Props;
  state: State;

  static defaultProps: {};

  static getStores() {
    return [ GlobalUiStore, FormsStore ];
  };

  static calculateState() {
    return {
      globalUi: GlobalUiStore.getState(),
      forms: FormsStore.getState()
    }
  };

  onSubmit = () => {
    const form = this.state.forms[createCandidateFormId];
    FormsActions.formValidate(createCandidateFormId);

    const isFormValid = Validations.validateAll([
      () => Validations.validateFilled(form.email) && Validations.validateEmail(form.email || ''),
      () => Validations.validateFilled(form.firstName, 3),
      () => Validations.validateFilled(form.lastName, 2),
      () => Validations.validateFilled(form.role, 2),
      () => Validations.validateFilled(form.seniority, 2)
    ]);

    if(isFormValid) {

        ServiceCandidate.createCandidate(
            form,
            (data) => {
                FormsActions.formReset(createCandidateFormId);
                this.props.history.push(Paths.user.candidates);
            },
            (error) => {
              GlobalUiActions.globalUiAsyncErrorUpdate({
                message: propAtPath(error, 'response.data.message'),
                actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
                autoHide: false,
                onRequestClose: function() {
                  GlobalUiActions.globalUiAsyncErrorReset();
                }
              });
            }
        );

    } else {
      GlobalUiActions.globalUiAsyncErrorUpdate({
        message: i18n.t('Pages.LoginPage.form.invalidForm'),
        actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
        autoHide: false,
        onRequestClose: function() {
          GlobalUiActions.globalUiAsyncErrorReset();
        }
     });

    }

  }

  handleEnterKeyDown = (event: KeyboardEvent) => {
    if(event.which === 13 || event.keyCode === 13){
      this.onSubmit();
    } else {
      /* do nothing */
    }
  }

  componentWillMount() {
    document.addEventListener('keydown', this.handleEnterKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleEnterKeyDown)
  }

  render() {
    const form = this.state.forms[createCandidateFormId];

    return(
      <div>
        <Toolbar noShadow={true}/>
        <div className="page AuthPage">
          <div className="AuthPage__logo">
          </div>
          <Paper zDepth={1} className="NewCandidatePage__paper">
            <h2>{i18n.t('Pages.NewCandidatePage.title')}</h2>
            <TextFieldWithValidation
              id="NewCandidatePage__firstName"
              value={form.firstName}
              fullWidth={true}
              multiLine={false}
              hintText=""
              errorText={i18n.t('Pages.NewCandidatePage.form.firstNameValidation')}
              floatingLabelText={i18n.t('Pages.NewCandidatePage.form.firstNameLabel')}
              showValidations={form.validated}
              fieldKey={'firstName'}
              focusedInput={form.focusedInput}
              validation={(value) => Validations.validateFilledMax(value,3,40)}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'firstName', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'firstName')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <TextFieldWithValidation
              id="NewCandidatePage__lastName"
              value={form.lastName}
              fullWidth={true}
              multiLine={true}
              hintText=""
              errorText={i18n.t('Pages.NewCandidatePage.form.lastNameValidation')}
              floatingLabelText={i18n.t('Pages.NewCandidatePage.form.lastNameLabel')}
              showValidations={form.validated}
              fieldKey={'lastName'}
              focusedInput={form.focusedInput}
              validation={(value) => Validations.validateFilledMax(value,2,40)}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'lastName', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'lastName')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <TextFieldWithValidation
              id="NewCandidatePage__email"
              value={form.email}
              fullWidth={true}
              multiLine={false}
              hintText=""
              errorText={i18n.t('Pages.NewCandidatePage.form.emailValidation')}
              floatingLabelText={i18n.t('Pages.NewCandidatePage.form.emailLabel')}
              showValidations={form.validated}
              fieldKey={'email'}
              type={'email'}
              focusedInput={form.focusedInput}
              validation={(value) => Validations.validateFilled(value) && Validations.validateEmail(value)}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'email', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'email')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <DatePicker
              value={form.dob}
              fullWidth={true}
              className="NewCandidatePage__dob"
              autoOk={true}
              floatingLabelText={i18n.t('Pages.NewCandidatePage.form.dobLabel')}
              mode={Helpers.isSmallScreen() ? "portrait" : "landscape"}
              onChange={(_, date) => FormsActions.formInputChange(createCandidateFormId, 'dob', date)}
              defaultDate={form.dob}
              formatDate={(date) => date && dateFns.format(date, 'DD/MM/YYYY')}
              okLabel={false}
              locale="it"
              DateTimeFormat={DateTimeFormat}
            />
            <TextFieldWithValidation
              id="NewCandidatePage__country"
              value={form.country}
              fullWidth={true}
              multiLine={false}
              hintText=""
              floatingLabelText={i18n.t('Pages.NewCandidatePage.form.countryLabel')}
              fieldKey={'country'}
              focusedInput={form.focusedInput}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'country', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'country')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <TextFieldWithValidation
              id="NewCandidatePage__role"
              value={form.role}
              fullWidth={true}
              multiLine={false}
              hintText=""
              floatingLabelText={i18n.t('Pages.NewCandidatePage.form.roleLabel')}
              errorText={i18n.t('Pages.NewCandidatePage.form.roleValidation')}
              fieldKey={'role'}
              showValidations={form.validated}
              validation={(value) => Validations.validateFilledMax(value,3,40)}
              focusedInput={form.focusedInput}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'role', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'role')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <TextFieldWithValidation
              id="NewCandidatePage__seniority"
              value={form.seniority}
              fullWidth={true}
              multiLine={false}
              hintText=""
              floatingLabelText={i18n.t('Pages.NewCandidatePage.form.seniorityLabel')}
              errorText={i18n.t('Pages.NewCandidatePage.form.seniorityValidation')}
              fieldKey={'seniority'}
              showValidations={form.validated}
              validation={(value) => Validations.validateFilledMax(value,3,40)}
              focusedInput={form.focusedInput}
              onChange={(event, value) => FormsActions.formInputChange(createCandidateFormId, 'seniority', value)}
              onFocus={(event) => { event.target.select(); FormsActions.formInputFocus(createCandidateFormId, 'seniority')}}
              onBlur={(event) => FormsActions.formBlur(createCandidateFormId)}
            />
            <RaisedButton
              className="NewCandidatePage__signup-button"
              label={i18n.t('Pages.NewCandidatePage.form.submit')}
              primary={true}
              fullWidth={true}
              onTouchTap={this.onSubmit}
            />
          </Paper>
        </div>
      </div>
    )
  };
}

export default Container.create(NewCandidatePage);
