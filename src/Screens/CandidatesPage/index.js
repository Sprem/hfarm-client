// @flow
import type { Candidates } from '../../Types/Candidate';
import type { RouterHistory } from 'react-router-dom';
import React, { Component } from 'react';
import { Container } from 'flux/utils';
import IconButton from 'material-ui/IconButton';
import { Paper, RaisedButton, FontIcon, TextField } from 'material-ui';
import { Tabs, Tab } from 'material-ui/Tabs';
import Toolbar from '../../Components/Toolbar';
// stores
import CandidateStore from '../../Stores/Candidate';
// actions
import GlobalUiActions from '../../Actions/GlobalUi';
// libs
import Paths from '../../App/Paths';
import { propAtPath } from '../../Services/Helpers';
import * as ServiceCandidate from '../../Services/Api/Candidate';
import i18n from 'i18next';
import { noop } from '../../Services/Helpers';
import './index.css';
import '../../Theme/boot-grid.css';

type Props = {
  history: RouterHistory
}

type State = {
  candidates: Candidates
}

class CandidatesPage extends Component {
  props: Props;
  state: State;

  filterType = "all";

  static defaultProps: {};

  static getStores() {
    return [ CandidateStore ];
  };

  static calculateState() {
    return {
      candidates: CandidateStore.getState()
    }
  };

  getCandidates = () => {
      console.log("GET CANDIDATE")
      setTimeout(function() {
          ServiceCandidate.listCandidate(
            noop,
            (error) => {
              GlobalUiActions.globalUiAsyncErrorUpdate({
                message: propAtPath(error, 'response.data.message_it'),
                actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
                autoHide: false,
                onRequestClose: function() {
                  GlobalUiActions.globalUiAsyncErrorReset();
                }
              });
            }
          )
      }, 0)
  }

  filterCandidate = () => {
      if(this.filterType == "all") return this.state.candidates.candidates;
      else if(this.filterType == "busy") return  this.state.candidates.candidates.filter((candidate) => candidate.isBusy == true);
      else return  this.state.candidates.candidates.filter((candidate) => candidate.isBusy == false);
  }

  deleteCandidate = (event: any) => {
      const idApplicant = event.target.id;
      setTimeout(function() {
          ServiceCandidate.deleteCandidate(
            idApplicant,
            noop,
            (error) => {
              GlobalUiActions.globalUiAsyncErrorUpdate({
                message: propAtPath(error, 'response.data.message_it'),
                actionMessage: i18n.t('Components.AsyncErrors.genericErrorConfirmationLabelOk'),
                autoHide: false,
                onRequestClose: function() {
                  GlobalUiActions.globalUiAsyncErrorReset();
                }
              });
            }
          )
      }, 0)
  }

  componentDidMount() {
      this.getCandidates();
  }

  goToUpdateCandidate = (event: any) => {
      const idApplicant = event.target.id;
      this.props.history.push(Paths.user.updateCandidate.replace(':id', idApplicant));
  }

  goToCreateCandidate = () => {
      this.props.history.push(Paths.user.createCandidate);
  }

  filterAll = () => {
      this.filterType = "all";
      this.forceUpdate();
  }

  filterBusy = () => {
      this.filterType = "busy";
      this.forceUpdate();
  }

  filterAvailable = () => {
      this.filterType = "available";
      this.forceUpdate();
  }

  render() {

    //const candidates = this.state.candidates.candidates;
    const candidates = this.filterCandidate();

    return(
        <div className="container">
            <Toolbar noShadow={true}/>
                <div className="page AuthPage">
                    <div className="AuthPage__logo">
                    </div>
                    <Paper zDepth={1} className="CandidatesPage__paper">

                        <div>
                            <div className="row">
                                <div className="CandidatesPage__top col-sm-12 col-md-6 col-lg-4">
                                    <RaisedButton
                                      style={{minWidth: '170px'}}
                                      label={i18n.t('Pages.CandidatesPage.newCandiate')}
                                      primary={true}
                                      fullWidth={true}
                                      onTouchTap={this.goToCreateCandidate}
                                    />
                                </div>
                                <div className="CandidatesPage__top col-sm-12 col-md-6 col-lg-4">
                                    <Tabs>
                                        <Tab label={i18n.t('Pages.CandidatesPage.tab.all')} onActive={this.filterAll}>
                                        </Tab>
                                        <Tab label={i18n.t('Pages.CandidatesPage.tab.busy')} onActive={this.filterBusy}>
                                        </Tab>
                                        <Tab label={i18n.t('Pages.CandidatesPage.tab.available')} onActive={this.filterAvailable}>
                                        </Tab>
                                    </Tabs>
                                </div>
                                <div className="CandidatesPage__top col-sm-0 col-md-0 col-lg-1"></div>
                                <div className="CandidatesPage__top col-sm-12 col-md-12 col-lg-3">
                                <TextField
                                  fullWidth={true}
                                  hintText={i18n.t('Pages.CandidatesPage.search')}
                                />
                                </div>
                            </div>
                        </div>

                        <div className="CandidatesPage__wrapperList">
                            <ul>
                                {

                                    candidates.map((candidate,index) => {
                                        return <div className="CandidatesPage__wrapperRow">
                                            { candidate.isBusy ?
                                                (candidate.isInternal ?
                                                    <div className="CandidatesPage__internal"></div>
                                                    :
                                                    <div className="CandidatesPage__external"></div>
                                                )
                                                :
                                                <div className="CandidatesPage__none"></div>
                                            }
                                            <div className="CandidatesPage__info content--vertical-centered">
                                                <p className="CandidatesPage__name" key={index + 100}>{candidate.firstName} {candidate.lastName}</p>
                                                <p className="CandidatesPage__role" key={index + 500}>{candidate.role}</p>
                                            </div>
                                            <div className="CandidatesPage__seniority content--vertical-centered">
                                                <span key={index + 1000}>{candidate.seniority}</span>
                                            </div>

                                            { candidate.isBusy ?
                                                <div className="CandidatesPage__job content--vertical-centered">
                                                    <p className="CandidatesPage__role" key={index + 3000}>{candidate.clientName}</p>
                                                    <p className="CandidatesPage__name" key={index + 4000}>{candidate.jobCallTitle.length > 18 ? candidate.jobCallTitle.slice(0,18)+"..." : candidate.jobCallTitle}</p>
                                                </div>
                                                :
                                                <div className="CandidatesPage__job content--vertical-centered">
                                                    <p>None</p>
                                                </div>
                                            }

                                            <div className="CandidatesPage__utils">
                                                <FontIcon style={{marginRight: '10px'}} className="material-icons content--vertical-centered" >description</FontIcon>
                                                <IconButton onTouchTap={this.deleteCandidate} style={{marginRight: '0px'}} className="material-icons content--vertical-centered" ><FontIcon style={{marginRight: '10px'}} id={candidate.idApplicant} className="material-icons content--vertical-centered" >delete</FontIcon></IconButton>
                                                <IconButton onTouchTap={this.goToUpdateCandidate} className="material-icons content--vertical-centered" ><FontIcon style={{marginRight: '10px'}} id={candidate.idApplicant} className="material-icons content--vertical-centered" >create</FontIcon></IconButton>
                                            </div>
                                            <div className="CandidatesPage__clear"></div>
                                        </div>


                                    })
                                }
                            </ul>
                        </div>

                    </Paper>
                </div>
        </div>
    )
  };
}

export default Container.create(CandidatesPage);
