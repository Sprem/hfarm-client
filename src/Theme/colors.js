const colors = {
  white: '#FFFFFF',
  petrol: '#515860',
  primary: '#F5C24D',
  secondary: '#E85D63',
  yellow: '#F9CA14',
  yellowLight: '#FFF59D',
  grey: '#747980',
  greyLight1: '#979BA0',
  greyLight2: '#D1D1D1',
  greyLight3: '#E8EBED',
  greyDark1: '#777777',
  fog: '#F1F3F4',
  blue: '#5298F3'
}

export default colors;
