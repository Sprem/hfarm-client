// @flow

import colors from './colors';

export const selectFieldIconStyle = {'padding': '0px', width: '32px', height: '32px', top: '16px', fill: colors.petrol};
