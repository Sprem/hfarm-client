// @flow

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import colors from '../Theme/colors';

export const muiTheme = getMuiTheme({
  palette: {
    primary1Color: colors.primary,
    textColor: colors.petrol,
    accent1Color: colors.primary
  },
  button: {
    height: 48
  },
  raisedButton: {
    fontSize: 16,
    fontWeight: 500
  },
  textField: {
    errorColor: colors.secondary
  },
  snackbar: {
    textColor: colors.white,
    actionColor: colors.yellow
  },
  datePicker: {
    selectColor: colors.primary,
    headerColor: colors.primary
  },
  tabs: {
    backgroundColor: colors.white,
    textColor: colors.greyDark1,
    selectedTextColor: colors.primary,
  },
  checkbox: {
    boxColor: colors.grey
  }
});
