// @flow

import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { CircularProgress } from 'material-ui';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import PrivateRoute from '../Components/PrivateRoute';
import Footer from '../Components/Footer';
import AsyncSpinner from '../Components/AsyncSpinner';
import AsyncErrors from '../Components/AsyncErrors';
// screens
import DashboardPage from '../Screens/DashboardPage';
import NewJobPage from '../Screens/NewJobPage';
import JobPage from '../Screens/JobPage';
import UpdateCandidatePage from '../Screens/UpdateCandidatePage';
import NewCandidatePage from '../Screens/NewCandidatePage';
import CandidatesPage from '../Screens/CandidatesPage';
import LoginPage from '../Screens/LoginPage';

// stores
import GlobalUiStore from '../Stores/GlobalUi';
// actions
import GlobalUiActions from '../Actions/GlobalUi';
// libs
import Paths from './Paths';
import '../I18n/index';
import { muiTheme } from './utils';
import Cookies from 'universal-cookie';

type State = {
    globalUi: GlobalUi
}

class App extends Component {
    state: State;
    static defaultProps: {};

    static getStores() {
        return [ GlobalUiStore ];
    }

    static calculateState() {
        return {
            globalUi: GlobalUiStore.getState()
        }
    }
    render() {
        const cookies = new Cookies();
        const authCookie = cookies.get('auth');
        const isAuthenticated = !!authCookie;
        return this.state.globalUi.isInitializing ?
            (<MuiThemeProvider muiTheme={muiTheme}><div className="AsyncSpinner"><CircularProgress /></div></MuiThemeProvider>)
            :
            (
                <MuiThemeProvider muiTheme={muiTheme}>
                  <BrowserRouter>
                    <div className="container">
                        <div className="content">
                        <Switch>
                            {/* Auth */}
                            <Route exact path={Paths.generic.index} component={LoginPage}/>
                            {/* -- User -- */}
                            <PrivateRoute isAuthenticated={isAuthenticated} exact path={Paths.user.dashboard} component={DashboardPage}/>
                            <PrivateRoute isAuthenticated={isAuthenticated} exact path={Paths.user.createJob} component={NewJobPage}/>
                            <PrivateRoute isAuthenticated={isAuthenticated} exact path={Paths.user.job} component={JobPage}/>
                            <PrivateRoute isAuthenticated={isAuthenticated} exact path={Paths.user.createCandidate} component={NewCandidatePage}/>
                            <PrivateRoute isAuthenticated={isAuthenticated} exact path={Paths.user.updateCandidate} component={UpdateCandidatePage}/>
                            <PrivateRoute isAuthenticated={isAuthenticated} exact path={Paths.user.candidates} component={CandidatesPage}/>
                          <Route component={(props) => <h1>Page not found</h1>}/>
                        </Switch>
                      </div>
                      <AsyncSpinner />
                      <AsyncErrors />
                      <Footer path={window.location.pathname} />
                    </div>
                  </BrowserRouter>
                </MuiThemeProvider>
            );
    }
}

export default Container.create(App);
