const Paths = {
  generic: {
    index: '/'
  },
  user: {
    dashboard: '/dashboard',
    candidates: '/candidati',
    createCandidate: '/candidato/crea',
    updateCandidate: '/candidato/:id/aggiorna',
    createJob: '/job/crea',
    job: '/job'
  }
}

export default Paths;
