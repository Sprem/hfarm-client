// @flow
import type { BusinessProfileSignupForm, BusinessProfileLoginForm, BusinessProfileForgotPasswordForm, BusinessProfileRestorePasswordForm } from './BusinessUser';
import type { NewTaskForm, NewTaskBuilderAction } from './NewTask';
import type { CompanyDetailsForm, BillingDetailsForm, PaymentDetailsForm, BusinessAccountDetailsForm } from './BusinessProfile';
import type { StudentProfileSignupForm, StudentProfileLoginForm } from './StudentUser';
import type { StudentDetailsForm, StudentAdditionalDetailsForm, StudentTaskPreferencesForm, StudentAccountDetailsForm, StudentTaskCoverLetterForm } from './StudentProfile';

export type Field = {
  id: string,
  kind: string,
  value?: any
}

export type Form = {
  id: string,
  focusedInput?: string,
  validated: boolean
}

export type Forms = {
  /* business */
  businessProfileLoginForm: BusinessProfileLoginForm,
  businessProfileSignupForm: BusinessProfileSignupForm,
  businessProfileForgotPasswordForm: BusinessProfileForgotPasswordForm,
  businessProfileRestorePasswordForm: BusinessProfileRestorePasswordForm,

  newTaskForm: NewTaskForm,

  companyDetailsForm: CompanyDetailsForm,
  billingDetailsForm: BillingDetailsForm,
  paymentDetailsForm: PaymentDetailsForm,
  businessAccountDetailsForm: BusinessAccountDetailsForm,
  /* student */
  studentProfileSignupForm: StudentProfileSignupForm,
  studentProfileLoginForm: StudentProfileLoginForm,
  studentDetailsForm: StudentDetailsForm,
  studentAdditionalDetailsForm: StudentAdditionalDetailsForm,
  studentTaskPreferencesForm: StudentTaskPreferencesForm,
  studentTaskCoverLetterForm: StudentTaskCoverLetterForm,
  studentAccountDetailsForm: StudentAccountDetailsForm
};

export type FormsActions = NewTaskBuilderAction;
