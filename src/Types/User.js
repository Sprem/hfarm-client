// @flow

import type { Form } from './Forms';
import type { RemoteDataStatus } from './RemoteDataStatus';

export type LoginForm = Form & {
  email?: string,
  password?: string
};

export type UserInfo = {
  id: string,
  firstName: string,
  lastName: string,
  email?: string,
  remoteDataStatus?: string
};

export type UserActionType =
| 'USER_INFO_UPDATE'
| 'USER_LOGIN_UPDATE_REMOTE_DATA_STATUS';

export type UserAction = {
 type: UserActionType,
 [key: ?string]: any
};
