// @flow

export type AsyncError = {
  visible?: boolean,
  message?: string,
  actionMessage?: string,
  autoHide: boolean,
  onRequestClose?: Function
};

export type StepperState = {
  id: string,
  activeStep: number
}

export type TabState = {
  id: string,
  activeTab: number
}

export type GlobalUi = {
  isInitializing: boolean,
  asyncErrors: AsyncError,
  steppers: {
    newTaskStepper: StepperState,
    businessOnboardingStepper: StepperState,
    studentOnboardingStepper: StepperState
  },
  visibleDialogs: {
    forgotPasswordPageSubmitSuccess: boolean,
    restorePasswordPageSubmitSuccess: boolean,
    invalidEmailDomainMessage: boolean,
    addCoverLetterForm: boolean,
    taskApplicantDetails: boolean,
    bizTosConfirmation: boolean,
  },
  tabs: {
    businessSettingsTab: TabState,
    studentSettingsTab: TabState
  },
  taskDetailsShowDetails: boolean,
  taskApplicantDetailsShowFullCoverLetter: boolean
};

export type GlobalUiActionType =
| 'GLOBAL_UI_IS_INITIALIZING_UPDATE'
| 'GLOBAL_UI_ASYNC_ERROR_UPDATE'
| 'GLOBAL_UI_ASYNC_ERROR_RESET'
| 'GLOBAL_UI_STEPPERS_UPDATE'
| 'GLOBAL_UI_STEPPERS_RESET'
| 'GLOBAL_UI_TABS_UPDATE'
| 'GLOBAL_UI_TABS_RESET'
| 'GLOBAL_UI_VISIBLE_DIALOG_UPDATE'
| 'GLOBAL_UI_TASK_DETAILS_SHOW_DETAILS_UPDATE'
| 'GLOBAL_UI_TASK_APPLICANT_DETAILS_SHOW_FULL_COVER_LETTER';

export type GlobalUiAction = {
 type: GlobalUiActionType,
 [key: ?string]: any
};
