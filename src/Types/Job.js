// @flow

import type { Form } from './Forms';
import type { RemoteDataStatus } from './RemoteDataStatus';

export type CreateJobForm = Form & {
  title?: string,
  isInternal?: boolean,
  clientName?: string,
  description?: string,
  startingDate?: string,
  closingDate?: string,
  experience?: string,
  remoteDataStatus?: string
};

export type Job = {
    idJobCall?: string,
    createdAt?: string,
    title?: string,
    idUser?: string
    clientName?: string
    isInternal?: boolean,
    description?: string,
    startingDate?: string,
    closingDate?: string,
    division?: string,
    experienceRequired?: string
}

export type Jobs = {
    jobs: Array<Job>
}

export type JobActionType =
| 'JOB_INFO_UPDATE'
| 'JOBS_UPDATE'
| 'JOB_UPDATE_REMOTE_DATA_STATUS';

export type JobActionTypeAction = {
 type: JobActionType,
 [key: ?string]: any
};
