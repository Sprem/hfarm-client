// @flow

import type { Form } from './Forms';
import type { RemoteDataStatus } from './RemoteDataStatus';

export type CreateCandidateForm = Form & {
  firstName?: string,
  lastName?: string,
  dob?: string,
  country?: string,
  email?: string,
  role?: string,
  seniority?: string,
  remoteDataStatus?: string
};

export type Candidate = {
    country?: string,
    createdAt?: string,
    cvPath?: string,
    dob?: string
    email?: string
    firstName?: string,
    idApplicant?: string,
    keywords?: string,
    lastName?: string,
    role?: string,
    seniority?: string,
    status?: string,
    clientName?: string,
    jobCallTitle?: string,
    idApplication?: string,
    isBusy?: boolean,
    isInternal?: boolean,
}

export type Candidates = {
    candidates: Array<Candidate>
}

export type CandidateActionType =
| 'CANDIDATE_INFO_UPDATE'
| 'CANDIDATES_UPDATE'
| 'CANDIDATE_UPDATE'
| 'CANDIDATE_UPDATE_REMOTE_DATA_STATUS';

export type CandidateAction = {
 type: CandidateActionType,
 [key: ?string]: any
};
