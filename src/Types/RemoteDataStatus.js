export type RemoteDataStatus =
  | 'NOT_ASKED'
  | 'LOADING'
  | 'FAILURE'
  | 'SUCCESS';
