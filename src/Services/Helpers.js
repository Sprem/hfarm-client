// @flow

const pathToPropsArray = (path: string): Array<string> =>
  path.split('.');

const pathToFirstProp = (path: string): string =>
  pathToPropsArray(path).slice(0,1).join();

const pathToPathTail = (path: string): string =>
  pathToPropsArray(path).slice(1).join('.');

/*
 * recursively deep merge an object with the given value at the given path
 * i.e. deepMerge({a: {b: false, c: false}}, 'a.b', true)// {a: {b: true, c: false}}
 */
export const deepMerge = (object: any, path: string, value: any): any =>
  pathToPropsArray(path).length > 1 ?
    Object.assign({}, object, {[pathToFirstProp(path)]: deepMerge(object[pathToFirstProp(path)], pathToPathTail(path), value)})
    :
    Object.assign({}, object, {[path]: value})

/*
 * recursively find prop at a given path
 * i.e. propAtPath({a: {b: {c: ['a','b','c']}}}, 'a.b.c.0') // 'a'
 */
export const propAtPath = (object: any, path: string): any => {
  if(pathToPropsArray(path).length > 1) {
    return object[pathToFirstProp(path)] ? propAtPath(object[pathToFirstProp(path)], pathToPathTail(path)) : undefined;
  } else {
    return object[path];
  }
};

/*
 * execute an array of function getting success and failure callbacks synchronously,
 * call onSuccess if all functions secceed, onError if any fails
 */
export const executeAllFunctions = (functions: Array<Function>, onSuccess: Function, onError: Function) => {
  functions.slice(0, 1)
  .map((func) => {
    return func(
      functions.length > 1 ? () => executeAllFunctions(functions.slice(1), onSuccess, onError) : onSuccess,
      onError
    )
  });
};

const _Array: any = Array;
const _Number: any = Number;

export const isSmallScreen = (): boolean => {
  return window.innerWidth <= 768;
};

const _document: any = document;

export const resetScrollPosition = ():void => {
  window.scrollTo(0, 0);
}

export const scrollToFirstInvalidInputField = (): void => {
  setTimeout(function() {
    const scrollTop = window.pageYOffset || _document.documentElement.scrollTop;
    const firstErrorField = _document.querySelector('.InputField--invalid');
    if(firstErrorField) {
      const firstErrorFieldOffsetPosition = firstErrorField.getBoundingClientRect().top + scrollTop;
      window.scrollTo(0, firstErrorFieldOffsetPosition - 50);
    } else {
      /* do nothing */
    }
  }, 0);
};

export const noop = () => {  /* do nothing */ };

export const getQueryVariable = (variable) => {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) === variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    return null;
}
