// @flow

import type { LoginForm } from '../../Types/User';
import RemoteDataStatus from '../RemoteDataStatus';
import Cookies from 'universal-cookie';

import axios from './index';
import UserActions from '../../Actions/User';
import remoteDataStatus from '../RemoteDataStatus';

const updateLoginRemoteDataStatus = (remoteDataStatus: RemoteDataStatus) => {
    UserActions.userLoginUpdateRemoteDataStatus(remoteDataStatus);
};

export const loginUser = (user: LoginForm, onSuccess: ?Function, onError: ?Function) => {
  updateLoginRemoteDataStatus(remoteDataStatus.LOADING);
  axios.post('v1/auth', {
    email: user.email,
    password: user.password
  }).then(function(response) {
    const cookies = new Cookies();
    cookies.set('auth', 'yes', { path: '/' });
    updateLoginRemoteDataStatus(remoteDataStatus.SUCCESS);
    onSuccess && onSuccess(response.data);
  }).catch(function(error) {
    updateLoginRemoteDataStatus(remoteDataStatus.FAILURE);
    onError && onError(error);
  });
};

/*export const logout = (onSuccess: ?Function, onError: ?Function, updateUiState: ?boolean) => {
  updateUiState && BusinessUserActions.businessUserLoginUpdateRemoteDataStatus(remoteDataStatus.LOADING);
  axios.delete('v1/auth')
  .then(function(response) {
    onSuccess && onSuccess(response);
    updateUiState && BusinessUserActions.businessUserLoginUpdateRemoteDataStatus(remoteDataStatus.SUCCESS);
  })
  .catch(function(error) {
    onError && onError(error);
    updateUiState && BusinessUserActions.businessUserLoginUpdateRemoteDataStatus(remoteDataStatus.FAILURE);
  });
}*/
