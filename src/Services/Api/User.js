// @flow
import type { LoginForm } from '../../Types/User';

import { loginUser } from './Auth';

export const loginUserAPI = (user: LoginForm, onSuccess: ?Function, onError: ?Function) => {
  loginUser(user, onSuccess, onError);
};
