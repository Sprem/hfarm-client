// @flow
import type { CreateCandidateForm, Candidate } from '../../Types/Candidate';
import remoteDataStatus from '../RemoteDataStatus';
import CandidateActions from '../../Actions/Candidate';

import axios from './index';

const updateCandidateRemoteDataStatus = (remoteDataStatus: RemoteDataStatus) => {
    CandidateActions.candidateUpdateRemoteDataStatus(remoteDataStatus);
};

export const responseToCandidate = (candidate: any): Candidate => ({
  country: candidate.country,
  createdAt: candidate.created_at,
  cvPath: candidate.cv_path,
  dob: candidate.dob,
  email: candidate.email,
  idApplicant: candidate.id_applicant,
  keywords: candidate.keywords,
  lastName: candidate.last_name,
  firstName: candidate.first_name,
  role: candidate.role,
  seniority: candidate.seniority,
  status: candidate.status,
  clientName: candidate.client_name,
  jobCallTitle: candidate.job_call_title,
  idApplication: candidate.id_application,
  isBusy: candidate.is_busy,
  isInternal: candidate.is_internal,
});

export const createCandidate = (candidate: CreateCandidateForm, onSuccess: ?Function, onError: ?Function) => {
  updateCandidateRemoteDataStatus(remoteDataStatus.LOADING);
  axios.post('v1/applicant', {
    first_name: candidate.firstName,
    last_name: candidate.lastName,
    dob: candidate.dob,
    country: candidate.country,
    email: candidate.email,
    role: candidate.role,
    seniority: candidate.seniority
  }).then(function(response) {
    updateCandidateRemoteDataStatus(remoteDataStatus.SUCCESS);
    onSuccess && onSuccess(response.data);
  }).catch(function(error) {
    updateCandidateRemoteDataStatus(remoteDataStatus.FAILURE);
    onError && onError(error);
  });
};

export const updateCandidate = (idApplicant: string, candidate: CreateCandidateForm, onSuccess: ?Function, onError: ?Function) => {
  updateCandidateRemoteDataStatus(remoteDataStatus.LOADING);
  axios.put(`v1/applicant/${idApplicant}`,{
      first_name: candidate.firstName,
      last_name: candidate.lastName,
      dob: candidate.dob,
      country: candidate.country ? candidate.country : undefined,
      email: candidate.email,
      role: candidate.role,
      seniority: candidate.seniority
  }).then(function(response) {
    updateCandidateRemoteDataStatus(remoteDataStatus.SUCCESS);
    onSuccess && onSuccess(response.data);
  }).catch(function(error) {
    updateCandidateRemoteDataStatus(remoteDataStatus.FAILURE);
    onError && onError(error);
  });
};

export const deleteCandidate = (idApplicant: string, onSuccess: ?Function, onError: ?Function) => {
  updateCandidateRemoteDataStatus(remoteDataStatus.LOADING);
  axios.delete(`v1/applicant/${idApplicant}`).then(function(response) {
    updateCandidateRemoteDataStatus(remoteDataStatus.SUCCESS);
    listCandidate("all");
    onSuccess && onSuccess(response.data);
  }).catch(function(error) {
    updateCandidateRemoteDataStatus(remoteDataStatus.FAILURE);
    onError && onError(error);
  });
};

export const listCandidate = (onSuccess: ?Function, onError: ?Function) => {
  updateCandidateRemoteDataStatus(remoteDataStatus.LOADING);
  axios.get('v1/applicant').then(function(response) {
      var result = [];
      response.data.map((candidate, index) =>
        result[index] = responseToCandidate(candidate)
      )
      CandidateActions.candidatesUpdate(result);
    updateCandidateRemoteDataStatus(remoteDataStatus.SUCCESS);
    onSuccess && onSuccess(response.data);
  }).catch(function(error) {
    updateCandidateRemoteDataStatus(remoteDataStatus.FAILURE);
    onError && onError(error);
  });
};

export const getCandidate = (idApplicant: string, onSuccess: ?Function, onError: ?Function) => {
  updateCandidateRemoteDataStatus(remoteDataStatus.LOADING);
  axios.get(`v1/applicant/${idApplicant}`).then(function(response) {
      var candidate = responseToCandidate(response.data);
    CandidateActions.candidateUpdate(candidate);
    updateCandidateRemoteDataStatus(remoteDataStatus.SUCCESS);
    onSuccess && onSuccess(response.data);
  }).catch(function(error) {
    updateCandidateRemoteDataStatus(remoteDataStatus.FAILURE);
    onError && onError(error);
  });
};
