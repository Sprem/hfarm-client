export const local = {
    name: 'local',
    host: 'http://api.local/api/',
    headers: {
        contentType: 'application/json'
    }
}

export const react = {

}

export const test = {

}

export const production = {
    
}

export const settings = {
    local,
    react,
    test,
    production
}

export const environment = settings[process.env.REACT_APP_ENV];
