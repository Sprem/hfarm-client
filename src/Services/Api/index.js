// @flow

import axios from 'axios';
import { environment } from './Settings';

axios.defaults.baseURL = environment.host;
axios.defaults.headers.common['Content-Type'] = environment.headers.contentType;
axios.defaults.withCredentials = true;

export default axios;
