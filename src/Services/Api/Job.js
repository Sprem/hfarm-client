// @flow
import type { CreateJobForm, Job } from '../../Types/Job';
import remoteDataStatus from '../RemoteDataStatus';
import JobActions from '../../Actions/Job';

import axios from './index';

const updateJobRemoteDataStatus = (remoteDataStatus: RemoteDataStatus) => {
    JobActions.jobUpdateRemoteDataStatus(remoteDataStatus);
};

export const responseToJob = (job: any): Job => ({
  idJobCall: job.id_job_call,
  createdAt: job.created_at,
  title: job.title,
  idUser: job.id_user,
  clientName: job.client_name,
  isInternal: job.is_internal,
  description: job.description,
  startingDate: job.starting_date,
  closingDate: job.closing_date,
  division: job.division,
  experienceRequired: job.experience_required
});

export const createJob = (job: CreateJobForm, onSuccess: ?Function, onError: ?Function) => {
  updateJobRemoteDataStatus(remoteDataStatus.LOADING);
  axios.post('v1/job', {
    title: job.title,
    category: job.isInternal ? 'internal' : 'external',
    description: job.description,
    starting_date: job.startingDate,
    closing_date: job.closingDate,
    experience: job.experience,
    client_name: job.clientName
  }).then(function(response) {
    updateJobRemoteDataStatus(remoteDataStatus.SUCCESS);
    onSuccess && onSuccess(response.data);
  }).catch(function(error) {
    updateJobRemoteDataStatus(remoteDataStatus.FAILURE);
    onError && onError(error);
  });
};

export const listJob = (onSuccess: ?Function, onError: ?Function) => {
  updateJobRemoteDataStatus(remoteDataStatus.LOADING);
  axios.get('v1/job').then(function(response) {
      var result = [];
      response.data.map((job, index) =>
        result[index] = responseToJob(job)
      )
      JobActions.jobsUpdate(result);
    updateJobRemoteDataStatus(remoteDataStatus.SUCCESS);
    onSuccess && onSuccess(response.data);
  }).catch(function(error) {
    updateJobRemoteDataStatus(remoteDataStatus.FAILURE);
    onError && onError(error);
  });
};
