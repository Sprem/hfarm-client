// @flow

export const validateAll = (validations: Array<Function>) =>
  validations
  .filter(validation => validation())
  .length === validations.length;

export const validateFilled = (value: ?string, minimumLength: number = 1): boolean =>
  !!(value && value.length >= minimumLength);

export const validateFilledMax = (value: ?string, minimumLength: number = 1, maximumLength: number = 250): boolean =>
  !!(value && value.length >= minimumLength && value.length < maximumLength);

export const validateNumber = (value: ?number): boolean =>
  typeof value === 'number';

export const validateEmail = (email: ?string): boolean => {
  if(email) {
    return !!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    //return !!email.match(/.+@.+\..+/g);
  } else {
    return false;
  }
};

const _URL = window.URL || window.webkitURL;
