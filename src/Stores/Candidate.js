// @flow

import type { Candidate, CandidateAction } from '../Types/Candidate';

import { ReduceStore } from 'flux/utils';
import CandidateActionTypes from '../ActionTypes/Candidate';
import Dispatcher from '../dispatcher';
import RemoteDataStatus from '../Services/RemoteDataStatus';

const candidate: Candidate = {
  candidates: []
}

class CandidateStore extends ReduceStore {
  constructor() {
    super(Dispatcher);
  }

  getInitialState() {
    return Object.assign({}, candidate)
  }

  reduce(state: Candidate, action: CandidateAction) {

    switch (action.type) {

      case CandidateActionTypes.CANDIDATES_UPDATE:
        state.candidates = action.candidates;
        return Object.assign({}, state);

      case CandidateActionTypes.CANDIDATE_UPDATE:
       state.candidates[0] = action.candidate;
       return Object.assign({}, state);

      default:
        return state;
    }
  }
}

export default new CandidateStore();
