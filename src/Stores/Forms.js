// @flow

import type { Forms, FormsActions } from '../Types/Forms';
import type { LoginForm } from '../Types/User';
import type { CreateJobForm } from '../Types/Job';
import type { CreateCandidateForm } from '../Types/Candidate';

import { ReduceStore } from 'flux/utils';
import FormsActionTypes from '../ActionTypes/Forms';
import Dispatcher from '../dispatcher';

// business
export const loginFormId = 'loginForm';
export const createJobFormId = 'createJobForm';
export const createCandidateFormId = 'createCandidateForm';


/*
 * business profile
 */

const createJobForm: CreteJobForm = {
   id: createJobFormId,
   focusedInput: undefined,
   validated: false,
   title: undefined,
   isInternal: true,
   clientName: undefined,
   description: undefined,
   startingDate: undefined,
   closingDate: undefined,
   experience: undefined
};

const createCandidateForm: CreteCandidateForm = {
   id: createCandidateFormId,
   focusedInput: undefined,
   validated: false,
   firstName: undefined,
   lastName: undefined,
   dob: undefined,
   country: undefined,
   email: undefined,
   role: undefined,
   seniority: undefined
};

const loginForm: LoginForm = {
   id: loginFormId,
   focusedInput: undefined,
   validated: false,
   email: undefined,
   password: undefined
};

const populateForm = (forms: Forms, formId: string, fields: any) => {
  const currentForm = forms[formId];
  forms[formId] = Object.keys(currentForm).reduce(
    (form, key) => {
      //form[key] = fields[key] || currentForm[key]
      form[key] = Object.keys(fields).indexOf(key) > -1 ? fields[key] : currentForm[key]
      return form;
    }
  , {});

  return forms;
}

const resetForm = (forms: Forms, formId: string, fields: any) => {
  const currentForm = forms[formId];
  forms[formId] = Object.keys(currentForm).reduce(
    (form, key) => {
      form[key] = key === 'id' ? form[key] : key === 'isInternal' ? form[key] = true : undefined
      return form;
    }
  , {});

  return forms;
}

const updateInput = (forms: Forms, formId: string, inputId: string, value) => {
  forms[formId][inputId] = value;
  return forms;
};

const updateFocusedInput = (forms: Forms, formId: string, inputId: ?string) => {
  forms[formId].focusedInput = inputId;
  return forms;
};

const validateForm = (forms: Forms, formId: string) => {
  forms[formId].validated = true;
  return forms;
};

class FormsStore extends ReduceStore {
  constructor() {
    super(Dispatcher);
  }

  getInitialState() {
    return {
      //[loginFormId]: businessProfileLoginForm,
      [loginFormId]: loginForm,
      [createJobFormId]: createJobForm,
      [createCandidateFormId]: createCandidateForm,

    }
  }

  reduce(state: Forms, action: FormsActions) {

    switch (action.type) {
      case FormsActionTypes.FORM_POPULATE:
        return Object.assign({}, populateForm(state, action.formId, action.fields));

      case FormsActionTypes.FORM_INPUT_CHANGE:
        return Object.assign({}, updateInput(state, action.formId, action.inputId, action.value));

      case FormsActionTypes.FORM_INPUT_FOCUS:
        return Object.assign({}, updateFocusedInput(state, action.formId, action.inputId));

      case FormsActionTypes.FORM_BLUR:
        return Object.assign({}, updateFocusedInput(state, action.formId, undefined));

      case FormsActionTypes.FORM_VALIDATE:
        return Object.assign({}, validateForm(state, action.formId));

      case FormsActionTypes.FORM_RESET:
        return Object.assign({}, resetForm(state, action.formId));

      default:
        return state;
    }
  }
}

export default new FormsStore();
