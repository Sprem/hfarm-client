// @flow

import type { GlobalUi, GlobalUiAction } from '../Types/GlobalUi';

import { ReduceStore } from 'flux/utils';
import GlobalUiActionTypes from '../ActionTypes/GlobalUi';
import Dispatcher from '../dispatcher';

// steppers
export const newTaskStepperId = 'newTaskStepper';
export const businessOnboardingStepperId = 'businessOnboardingStepper';
export const studentOnboardingStepperId = 'studentOnboardingStepper';
// tabs
export const businessSettingsTabId = 'businessSettingsTab';
export const studentSettingsTabId = 'studentSettingsTab';
// dialogs
export const forgotPasswordPageSubmitSuccessId = 'forgotPasswordPageSubmitSuccess';
export const restorePasswordPageSubmitSuccessId = 'restorePasswordPageSubmitSuccess';
export const invalidEmailDomainMessageId = 'invalidEmailDomainMessage';
export const addCoverLetterFormId = 'addCoverLetterForm';
export const applicationConfirmationFormId = 'applicationConfirmationForm';
export const postTaskTutorialFormId = 'postTaskTutorialForm';
export const unlockContactsFormId = 'unlockContactsForm';
export const taskDetailsRecapDialogId = 'contactsWarningForm';
export const showBizTosDialogId = 'showBizTosDialog';
export const showStudentTosDialogId = 'showStudentTosDialog';
export const taskApplicantDetailsId = 'taskApplicantDetails';
export const addPaymentCardId = 'addPaymentCard';
export const activationPaymentWarningDialogId = 'activationPaymentWarningDialog';

const globalUi: GlobalUi = {
  isInitializing: false,
  asyncErrors: {
    visible: false,
    message: undefined,
    actionMessage: undefined,
    autoHide: false,
    onRequestClose: undefined
  },
  steppers: {
    [newTaskStepperId]: { id: newTaskStepperId, activeStep: 0 },
    [businessOnboardingStepperId]: { id: businessOnboardingStepperId, activeStep: 0 },
    [studentOnboardingStepperId]: {  id: studentOnboardingStepperId, activeStep: 0 }
  },
  tabs: {
    [businessSettingsTabId]: { id: businessSettingsTabId, activeTab: 0 },
    [studentSettingsTabId]: { id: studentSettingsTabId, activeTab: 0 }
  },
  visibleDialogs: {
    [forgotPasswordPageSubmitSuccessId]: false,
    [restorePasswordPageSubmitSuccessId]: false,
    [invalidEmailDomainMessageId]: false,
    [addCoverLetterFormId]: false,
    [applicationConfirmationFormId]: false,
    [postTaskTutorialFormId]: false,
    [unlockContactsFormId]: false,
    [taskDetailsRecapDialogId]: false,
    [showBizTosDialogId]: false,
    [taskApplicantDetailsId]: false,
    [activationPaymentWarningDialogId]: false
  },
  taskDetailsShowDetails: false,
  taskApplicantDetailsShowFullCoverLetter: false
}

class GlobalUiStore extends ReduceStore {
  constructor() {
    super(Dispatcher);
  }

  getInitialState() {
    return Object.assign({}, globalUi)
  }

  reduce(state: GlobalUi = globalUi, action: GlobalUiAction) {
    switch (action.type) {
      case GlobalUiActionTypes.GLOBAL_UI_IS_INITIALIZING_UPDATE:
        state.isInitializing = action.isInitializing
        return Object.assign({}, state);

      case GlobalUiActionTypes.GLOBAL_UI_ASYNC_ERROR_UPDATE:
        state.asyncErrors = Object.assign({visible: true}, action.asyncError);
        return Object.assign({}, state);

      case GlobalUiActionTypes.GLOBAL_UI_ASYNC_ERROR_RESET:
        state.asyncErrors = Object.assign({}, globalUi.asyncErrors);
        return Object.assign({}, state);

      case GlobalUiActionTypes.GLOBAL_UI_STEPPERS_UPDATE:
        state.steppers[action.stepperId].activeStep = action.activeStep;
        return Object.assign({}, state);

      case GlobalUiActionTypes.GLOBAL_UI_STEPPERS_RESET:
        state.steppers[action.stepperId].activeStep = 0;
        return Object.assign({}, state);

      case GlobalUiActionTypes.GLOBAL_UI_TABS_UPDATE:
        state.tabs[action.tabId].activeTab = action.activeTab;
        return Object.assign({}, state);

      case GlobalUiActionTypes.GLOBAL_UI_TABS_RESET:
        state.tabs[action.tabId].activeTab = 0;
        return Object.assign({}, state);

      case GlobalUiActionTypes.GLOBAL_UI_VISIBLE_DIALOG_UPDATE:
        state.visibleDialogs[action.dialogId] = action.isVisible;
        return Object.assign({}, state);

      case GlobalUiActionTypes.GLOBAL_UI_TASK_DETAILS_SHOW_DETAILS_UPDATE:
        state.taskDetailsShowDetails = action.showDetails;
        return Object.assign({}, state);

      case GlobalUiActionTypes.GLOBAL_UI_TASK_APPLICANT_DETAILS_SHOW_FULL_COVER_LETTER:
        state.taskApplicantDetailsShowFullCoverLetter = action.showFullCoverLetter;
        return Object.assign({}, state);

      default:
        return state;
    }
  }
}

export default new GlobalUiStore();
