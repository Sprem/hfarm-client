// @flow

import type { Jobs, JobAction } from '../Types/Job';

import { ReduceStore } from 'flux/utils';
import JobActionTypes from '../ActionTypes/Job';
import Dispatcher from '../dispatcher';
import RemoteDataStatus from '../Services/RemoteDataStatus';

const jobs: Jobs = {
  jobs: []
}

class JobStore extends ReduceStore {
  constructor() {
    super(Dispatcher);
  }

  getInitialState() {
    return Object.assign({}, jobs)
  }

  reduce(state: Jobs = jobs, action: JobAction) {

    switch (action.type) {

      case JobActionTypes.JOBS_UPDATE:
        state.jobs = action.jobs;
        return Object.assign({}, state);

      default:
        return state;
    }
  }
}

export default new JobStore();
