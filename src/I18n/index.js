// @flow

import i18n from 'i18next';
import it from './it.json';

i18n.init({
  lng: 'it',
  resources: {
    it: {
      translation: it
    }
  }
}, (err, t) => {
});
