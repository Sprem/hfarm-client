// @flow

import type { RemoteDataStatus } from '../Types/RemoteDataStatus';
import type { UserInfo } from '../Types/User';

import UserActionTypes from '../ActionTypes/User';
import Dispatcher from '../dispatcher';

const Actions = {
  userInfoUpdate(info: BusinessUserInfo) {
    Dispatcher.dispatch({
      type:  UserActionTypes.USER_INFO_UPDATE,
      info: info
    });
  },

  userLoginUpdateRemoteDataStatus(remoteDataStatus: RemoteDataStatus) {
    Dispatcher.dispatch({
      type:  UserActionTypes.USER_LOGIN_UPDATE_REMOTE_DATA_STATUS,
      remoteDataStatus: remoteDataStatus
    });
  }

};

export default Actions;
