// @flow

import FormsActionTypes from '../ActionTypes/Forms';
import Dispatcher from '../dispatcher';

const Actions = {
  formPopulate(formId: string, fields: any) {
    Dispatcher.dispatch({
      type:  FormsActionTypes.FORM_POPULATE,
      formId: formId,
      fields: fields
    });
  },

  formInputChange(formId: string, inputId: string, value: any) {
    Dispatcher.dispatch({
      type:  FormsActionTypes.FORM_INPUT_CHANGE,
      formId: formId,
      inputId: inputId,
      value: value
    });
  },

  formInputFocus(formId: string, inputId: string) {
    setTimeout(function() {
      Dispatcher.dispatch({
        type:  FormsActionTypes.FORM_INPUT_FOCUS,
        formId: formId,
        inputId: inputId
      });
    }, 0);
  },

  formBlur(formId: string) {
    Dispatcher.dispatch({
      type:  FormsActionTypes.FORM_BLUR,
      formId: formId
    });
  },

  formValidate(formId: string) {
    Dispatcher.dispatch({
      type:  FormsActionTypes.FORM_VALIDATE,
      formId: formId
    });
  },

  formReset(formId: string) {
    Dispatcher.dispatch({
      type:  FormsActionTypes.FORM_RESET,
      formId: formId
    });
  }
};

export default Actions;
