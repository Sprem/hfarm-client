// @flow

import type { RemoteDataStatus } from '../Types/RemoteDataStatus';
import type { Candidate } from '../Types/Candidate';

import CandidateActionTypes from '../ActionTypes/Candidate';
import FormsActionTypes from '../ActionTypes/Forms';
import Dispatcher from '../dispatcher';

import { createCandidateFormId } from '../Stores/Forms.js';

const Actions = {

  candidateUpdateRemoteDataStatus(remoteDataStatus: RemoteDataStatus) {
    Dispatcher.dispatch({
      type:  CandidateActionTypes.CANDIDATE_UPDATE_REMOTE_DATA_STATUS,
      remoteDataStatus: remoteDataStatus
    });
  },

  candidatesUpdate(candidates: Array<Candidate>) {
    Dispatcher.dispatch({
      type:  CandidateActionTypes.CANDIDATES_UPDATE,
      candidates: candidates
    });
  },

  candidateUpdate(candidate: Candidate) {
    Dispatcher.dispatch({
      type:  CandidateActionTypes.CANDIDATE_UPDATE,
      candidate: candidate
    });
    Dispatcher.dispatch({
      type:  FormsActionTypes.FORM_POPULATE,
      formId: createCandidateFormId,
      fields: {
          focusedInput: undefined,
          validated: false,
          firstName: candidate.firstName,
          lastName: candidate.lastName,
          dob: candidate.dob,
          country: candidate.country,
          email: candidate.email,
          role: candidate.role,
          seniority: candidate.seniority
      }
    });

  }

};

export default Actions;
