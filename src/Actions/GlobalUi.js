// @flow

import type { AsyncError } from '../Types/GlobalUi';

import GlobalUiActionTypes from '../ActionTypes/GlobalUi';
import Dispatcher from '../dispatcher';

const Actions = {
  globalUiIsInitializingUpdate(isInitializing: boolean) {
    Dispatcher.dispatch({
      type:  GlobalUiActionTypes.GLOBAL_UI_IS_INITIALIZING_UPDATE,
      isInitializing: isInitializing
    });
  },

  globalUiAsyncErrorUpdate(asyncError: AsyncError) {
    Dispatcher.dispatch({
      type:  GlobalUiActionTypes.GLOBAL_UI_ASYNC_ERROR_UPDATE,
      asyncError: asyncError
    });
  },

  globalUiAsyncErrorReset() {
    Dispatcher.dispatch({
      type:  GlobalUiActionTypes.GLOBAL_UI_ASYNC_ERROR_RESET
    });
  }

};

export default Actions;
