// @flow

import type { RemoteDataStatus } from '../Types/RemoteDataStatus';
import type { Job } from '../Types/Job';

import JobActionTypes from '../ActionTypes/Job';
import Dispatcher from '../dispatcher';

const Actions = {

  jobUpdateRemoteDataStatus(remoteDataStatus: RemoteDataStatus) {
    Dispatcher.dispatch({
      type:  JobActionTypes.JOB_UPDATE_REMOTE_DATA_STATUS,
      remoteDataStatus: remoteDataStatus
    });
  },

  jobsUpdate(jobs: Array<Job>) {
    Dispatcher.dispatch({
      type:  JobActionTypes.JOBS_UPDATE,
      jobs: jobs
    });
  }

};

export default Actions;
