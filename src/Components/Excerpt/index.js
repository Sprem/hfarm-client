// @flow

import React from 'react';
import './index.css';

type Props = {
  text: string,
  onShowAll: Function,
  onShowLess: Function,
  showAll: boolean,
  showAllLabel: string,
  showLessLabel: string,
  excerptLength: number,
  className?: string
}

const Excerpt = (props: Props) => {
  const text =  props.text || '';
  const shouldShowShowMoreButton = !props.showAll && text.split(' ').length > props.excerptLength;
  const shouldShowShowLessButton = props.showAll && text.split(' ').length > props.excerptLength;

  if(shouldShowShowMoreButton) {
    return <p className={`Excerpt ${props.className || '' }`}>{text.split(' ').slice(0, props.excerptLength).join(' ')} <a className="Excerpt__link" onClick={props.onShowAll}>{props.showAllLabel}</a></p>
  } else if(shouldShowShowLessButton) {
    return <p className={`Excerpt ${props.className || '' }`}>{text} <a className="Excerpt__link" onClick={props.onShowLess}>{props.showLessLabel}</a></p>
  } else {
    return <p className={`Excerpt ${props.className || '' }`}>{text}</p>
  }
}


export default Excerpt;
