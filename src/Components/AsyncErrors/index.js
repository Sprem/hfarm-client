// @flow
import type { GlobalUi } from '../../Types/GlobalUi';

import React, { Component } from 'react';
import { Container } from 'flux/utils';
import GlobalUiStore from '../../Stores/GlobalUi';
import { Snackbar } from 'material-ui';
import { noop } from '../../Services/Helpers';
import './index.css';

type State = {
  globalUi: GlobalUi
}

class AsyncErrors extends Component {
  state: State;

  static defaultProps: {};

  static getStores() {
    return [ GlobalUiStore ];
  }

  static calculateState() {
    return {
      globalUi: GlobalUiStore.getState()
    }
  }

  render() {
    const message = this.state.globalUi.asyncErrors.message || '';
    const action = this.state.globalUi.asyncErrors.actionMessage || '';
    const onRequestClose = this.state.globalUi.asyncErrors.onRequestClose ? this.state.globalUi.asyncErrors.onRequestClose : noop;
    return (
      this.state.globalUi.asyncErrors.autoHide ?
        <Snackbar
          className="AsyncErrors"
          open={this.state.globalUi.asyncErrors.visible}
          message={message}
          autoHideDuration={4000}
          onRequestClose={onRequestClose}
          action={action}
          onActionTouchTap={onRequestClose}
        />
        :
        <Snackbar
          className="AsyncErrors"
          open={this.state.globalUi.asyncErrors.visible}
          message={message}
          onRequestClose={onRequestClose}
          action={action}
          onActionTouchTap={onRequestClose}
        />
    )
  }
}

export default Container.create(AsyncErrors);
