// @flow

import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { Link } from 'react-router-dom';
import { AppBar } from 'material-ui';
import colors from '../../Theme/colors';
//store
import FormsStore from '../../Stores/Forms';
// libs
import Paths from '../../App/Paths';
import './index.css';
import Cookies from 'universal-cookie';

type Props = {
  noShadow?: boolean
}

type State = {
  forms: FormsStore
}

class Toolbar extends Component {
    state: State;
    props: Props;

    static getStores() {
      return [ FormsStore ];
    }

    static calculateState() {
      return { forms: FormsStore.getInitialState() }
    }

  logout = () => {
      const cookies = new Cookies();
      cookies.remove('auth', { path: '/' });
  }

  render() {

    return(

      <AppBar
        className={`Toolbar ${this.props.noShadow ? 'Toolbar--no-shadow' : ''}`}
        title={
            <div
              id="toolbar__go-to-home"
              >
              <div style={{'marginTop': '15px'}}></div>
              <h1 style={{'color': colors.white}}>MISTRAL</h1>
            </div>
        }
        showMenuIconButton={false}
        style={{'backgroundColor': colors.primary}}
        iconElementRight={
          <div style={{'marginTop': '8px'}} className="Toolbar__right-icons">
            <Link id="Toolbar__goToCandidate" to={Paths.user.candidates}>
              <span style={{'color': colors.white}}>CANDIDATI</span>
            </Link>
            <Link id="Toolbar__goToDashboard" to={Paths.user.dashboard}>
              <span style={{'color': colors.white}}>CALLS</span>
            </Link>
            <Link onTouchTap={this.logout} to={Paths.generic.index}>
                <span style={{'color': colors.white}}>LOGOUT</span>
            </Link>
          </div>
        }
      />
    )
  }
}

export default Container.create(Toolbar);
