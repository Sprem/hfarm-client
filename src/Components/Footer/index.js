// @flow

import React from 'react';
import i18n from 'i18next';
import './index.css';
import Paths from '../../App/Paths';

type Props = {
    path: string
};

const Footer = (props: Props) =>
  <footer className="footer">
      <p className="footer__text">

          {i18n.t('Components.Footer.copyright')}&nbsp;&nbsp;&nbsp;·
          &nbsp;&nbsp;&nbsp;
          <a href="mailto:mail@mail.com">{i18n.t('Components.Footer.conctactUs')}</a>
      </p>
  </footer>

export default Footer;
