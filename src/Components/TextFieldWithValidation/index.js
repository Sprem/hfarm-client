// @flow

import React from 'react';
import { TextField } from 'material-ui';
import { noop } from '../../Services/Helpers';

type Props = {
  id: ?string,
  value: any,
  floatingLabelText: string,
  hintText: string,
  errorText: boolean | string,
  helpText?: string,
  fullWidth: boolean,
  multiLine: boolean,
  rows?: number,
  rowsMax?: number,
  showValidations: boolean,
  fieldKey: string,
  type?: string,
  focusedInput?: string,
  disabled?: boolean,
  underlineDisabledStyle?: any,
  validation: Function,
  onFocus?: Function,
  onBlur?: Function,
  onChange?: Function
}

const showValidationInvalidMessage = (value: any, fieldKey: string, focusedInput: ?string, validation: Function, showValidations: boolean): boolean =>
  showValidations && fieldKey !== focusedInput && !validation(value);


const TextFieldWithValidation = (props: Props) => {
  const shouldShowInvalidMessage = showValidationInvalidMessage(props.value, props.fieldKey, props.focusedInput, props.validation, props.showValidations);
  return (
    <div className={`InputField ${shouldShowInvalidMessage ? 'InputField--invalid' : ''}`}>
      <TextField
        id={props.id}
        type={props.type || 'text'}
        fullWidth={props.fullWidth}
        multiLine={props.multiLine}
        rows={props.rows  || 1}
        rowsMax={props.rowsMax || 9999}
        hintText={props.hintText}
        errorText={shouldShowInvalidMessage && (props.errorText || " ")}
        floatingLabelText={props.floatingLabelText}
        value={props.value}
        onChange={props.onChange ? props.onChange : noop}
        onFocus={props.onFocus ? props.onFocus : noop}
        onBlur={props.onBlur ? props.onBlur : noop}
        disabled={props.disabled}
        underlineDisabledStyle={props.underlineDisabledStyle}
      />
      {props.helpText &&
        <span className={`TextField-helper ${shouldShowInvalidMessage ? 'TextField-helper--invalid' : ''}`}>{props.helpText}</span>
      }
    </div>
  )
}

export default TextFieldWithValidation;
