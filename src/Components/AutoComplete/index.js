import React, { Component } from 'react';
import * as MaterialUi from 'material-ui';
import TextFieldWithValidation from '../TextFieldWithValidation';
import ReactAutocomplete from 'react-autocomplete';
import { propAtPath,  } from '../../Services/Helpers';

const isIOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

function triggerFocus(event: any) {
  const element = event.target;
  element.blur();
  element.removeEventListener('focus', triggerFocus);
  setTimeout(function() {
    element.focus();
    element.addEventListener('focus', triggerFocus);
  }, 0);
}

class AutoComplete extends Component {
  constructor(props) {
    super(props);
    this.state = { showItems: false };
  }

  get dataSource() {
    return this.props.dataSource.filter(
      item =>
        this.props.filter ?
        this.props.filter(this.props.searchText || '', propAtPath(this.props, 'dataSourceConfig.text') ? item[this.props.dataSourceConfig.text] : item)
        :
        true
    )
    .slice(0, this.props.maxSearchResults || 10);
  }

  showItems = () => {
    this.setState({showItems: true});
  }

  hideItems = () => {
    this.setState({showItems: false});
  }

  componentDidMount() {
    if(isIOS) {
      setTimeout(() => {
        this.autocompleteWrapper.querySelector('input').addEventListener('focus', this.showItems);
      }, 0);
    } else {
      /* do nothing */
    }
  }

  componentWillUnmount() {
    if(isIOS) {
      this.autocompleteWrapper.querySelector('input').removeEventListener('focus', this.showItems);
    } else {
      /* do nothing */
    }
  }

  render() {
    if(isIOS) {
      return (
        <div className="Autocomplete" ref={autocompleteWrapper => this.autocompleteWrapper = autocompleteWrapper }>
          <ReactAutocomplete
            value={this.props.searchText}
            items={this.dataSource}
            getItemValue={(item) => this.props.dataSourceConfig ? item[this.props.dataSourceConfig.text] : item}
            onChange={(event) => this.props.onUpdateInput(event.target.value) }
            onSelect={(value) => {
              this.props.onUpdateInput(value);
              this.hideItems();
              setTimeout(() => {
                this.props.onNewRequest(value);
                this.props.onClose && this.props.onClose();
              }, 10);
            }}
            open={!!this.props.searchText && !!this.dataSource && !!this.dataSource.length && this.state.showItems}
            selectOnBlur={true}
            renderInput={(props) =>
              <TextFieldWithValidation
                fieldKey={'Autocomplete__input'}
                fullWidth={true}
                className={this.props.className}
                errorText={this.props.errorText}
                showValidations={this.props.showValidations}
                validation={() => !this.props.errorText}
                floatingLabelText={this.props.floatingLabelText}
                {...props}
              />
            }
            renderItem={(item, isHighlighted) =>
              <div className="Autocomplete__item" style={{ background: isHighlighted ? 'rgba(10, 10, 10, 0.2)' : 'white' }}>
                {this.props.dataSourceConfig ? item[this.props.dataSourceConfig.text] : item}
              </div>
            }
            renderMenu={(items, value, style) => <div className="Autocomplete__items" children={items}/>}
          />
        </div>
      );
    } else {
      return (
        <MaterialUi.AutoComplete {...this.props} />
      );
    }
  }
};

export default AutoComplete;
