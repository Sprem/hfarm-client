// @flow
import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...args }: any) => (
  <Route {...args} render={(props) => (
     args.isAuthenticated ? (
         args.tab ?
            <Component {...props} tab={args.tab}/>
         :
             <Component {...props} />
    ) : (
      <Redirect to={{
        pathname: args.isStudent ? '/studente/accedi' : '/accedi',
        state: { from: props.location }
      }}/>
    )
  )}/>
);

export default PrivateRoute;
