// @flow
import type { Candidate } from '../../Types/Candidate';
import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { CircularProgress } from 'material-ui';
/* stores */
import CandidateStore from '../../Stores/Candidate';
// libs
import RemoteDataStatus from '../../Services/RemoteDataStatus';
import './index.css';

type State = {
  candidate: Candidate
}

class AsyncSpinner extends Component {
  state: State;

  static defaultProps: {};

  static getStores() {
    return [ CandidateStore ];
  }

  static calculateState() {
    return {
      candidate: CandidateStore.getState()
    }
  }

  shouldSpinnerAppear() {
    return [
      //candidate
      this.state.candidate.updateRemoteDataStatus
    ]
    .filter((remoteDataStatus) => remoteDataStatus === RemoteDataStatus.LOADING)
    .length > 0
  }

  render() {
    return this.shouldSpinnerAppear() ? <div className="AsyncSpinner"><CircularProgress /></div> : <div />
  }
}

export default Container.create(AsyncSpinner);
