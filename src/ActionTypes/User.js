const user = {
  USER_INFO_UPDATE: 'USER_INFO_UPDATE',
  USER_LOGIN_UPDATE_REMOTE_DATA_STATUS: 'USER_LOGIN_UPDATE_REMOTE_DATA_STATUS'
}

export default user;
