const globalUi = {
  GLOBAL_UI_IS_INITIALIZING_UPDATE: 'GLOBAL_UI_IS_INITIALIZING_UPDATE',
  GLOBAL_UI_ASYNC_ERROR_UPDATE: 'GLOBAL_UI_ASYNC_ERROR_UPDATE',
  GLOBAL_UI_ASYNC_ERROR_RESET: 'GLOBAL_UI_ASYNC_ERROR_RESET',
  GLOBAL_UI_STEPPERS_UPDATE: 'GLOBAL_UI_STEPPERS_UPDATE',
  GLOBAL_UI_STEPPERS_RESET: 'GLOBAL_UI_STEPPERS_RESET',
  GLOBAL_UI_TABS_UPDATE: 'GLOBAL_UI_TABS_UPDATE',
  GLOBAL_UI_TABS_RESET: 'GLOBAL_UI_TABS_RESET',
  GLOBAL_UI_VISIBLE_DIALOG_UPDATE: 'GLOBAL_UI_VISIBLE_DIALOG_UPDATE',
  GLOBAL_UI_TASK_DETAILS_SHOW_DETAILS_UPDATE: 'GLOBAL_UI_TASK_DETAILS_SHOW_DETAILS_UPDATE',
  GLOBAL_UI_TASK_APPLICANT_DETAILS_SHOW_FULL_COVER_LETTER: 'GLOBAL_UI_TASK_APPLICANT_DETAILS_SHOW_FULL_COVER_LETTER'
};

export default globalUi;
