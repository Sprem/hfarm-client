# Mistral Client

### Setup
on project folder:
>$ yarn

⚠️ Follow [these steps](https://yarnpkg.com/en/docs/install) if yarn is not available on your terminal.

#### Development mode
Expose the development build at [http://localhost:3000/](http://localhost:3000/)
> $ yarn start

#### Build for production
Create the production build (which will be available in the `./build` folder)
> $ yarn run build

## Development
To avoid cors issues on localhost disable cors on your browser, for exemple if you use Google Chrome on macOS open it by running the following command:
```
$ open /Applications/Google\ Chrome.app --args --disable-web-security --user-data-dir
```

### React
This is a React application, more information [here](https://facebook.github.io/react/).

### Flux
Application structure and data flow based on standard Flux architecture
> Action -> Dispatcher -> Store -> View

find more [here](https://facebook.github.io/flux/docs/in-depth-overview.html#content).

### Navigation
Routing managed with [react-router](https://reacttraining.com/react-router/web/guides/quick-start).

### Flow
Flow is a static type checker for Javascript, more information [here](https://flow.org/).

## Test
For testing we are using selenium webdriver.
Chrome is the target browser, so make sure to have WebDriver for Chrome available on your machine (driver available [here](https://sites.google.com/a/chromium.org/chromedriver/downloads)).

### Run test
1. serve production build
  > $ yarn run serve

2. build and run test
  > $ yarn run test

### Add test
Add a test file under `./Test` folder;
more info [here](http://seleniumhq.github.io/selenium/docs/api/javascript/).
